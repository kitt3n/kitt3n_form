<?php
namespace KITT3N\Kitt3nForm\Tests\Unit\Domain\Model;

/**
 * Test case.
 */
class FormTest extends \TYPO3\CMS\Core\Tests\UnitTestCase
{
    /**
     * @var \KITT3N\Kitt3nForm\Domain\Model\Form
     */
    protected $subject = null;

    protected function setUp()
    {
        parent::setUp();
        $this->subject = new \KITT3N\Kitt3nForm\Domain\Model\Form();
    }

    protected function tearDown()
    {
        parent::tearDown();
    }

    /**
     * @test
     */
    public function getTitleReturnsInitialValueForString()
    {
        self::assertSame(
            '',
            $this->subject->getTitle()
        );
    }

    /**
     * @test
     */
    public function setTitleForStringSetsTitle()
    {
        $this->subject->setTitle('Conceived at T3CON10');

        self::assertAttributeEquals(
            'Conceived at T3CON10',
            'title',
            $this->subject
        );
    }

    /**
     * @test
     */
    public function getDescriptionReturnsInitialValueForString()
    {
        self::assertSame(
            '',
            $this->subject->getDescription()
        );
    }

    /**
     * @test
     */
    public function setDescriptionForStringSetsDescription()
    {
        $this->subject->setDescription('Conceived at T3CON10');

        self::assertAttributeEquals(
            'Conceived at T3CON10',
            'description',
            $this->subject
        );
    }

    /**
     * @test
     */
    public function getTextSubmitReturnsInitialValueForString()
    {
        self::assertSame(
            '',
            $this->subject->getTextSubmit()
        );
    }

    /**
     * @test
     */
    public function setTextSubmitForStringSetsTextSubmit()
    {
        $this->subject->setTextSubmit('Conceived at T3CON10');

        self::assertAttributeEquals(
            'Conceived at T3CON10',
            'textSubmit',
            $this->subject
        );
    }

    /**
     * @test
     */
    public function getTextConfirmReturnsInitialValueForString()
    {
        self::assertSame(
            '',
            $this->subject->getTextConfirm()
        );
    }

    /**
     * @test
     */
    public function setTextConfirmForStringSetsTextConfirm()
    {
        $this->subject->setTextConfirm('Conceived at T3CON10');

        self::assertAttributeEquals(
            'Conceived at T3CON10',
            'textConfirm',
            $this->subject
        );
    }

    /**
     * @test
     */
    public function getFieldSenderFirstNameReturnsInitialValueForString()
    {
        self::assertSame(
            '',
            $this->subject->getFieldSenderFirstName()
        );
    }

    /**
     * @test
     */
    public function setFieldSenderFirstNameForStringSetsFieldSenderFirstName()
    {
        $this->subject->setFieldSenderFirstName('Conceived at T3CON10');

        self::assertAttributeEquals(
            'Conceived at T3CON10',
            'fieldSenderFirstName',
            $this->subject
        );
    }

    /**
     * @test
     */
    public function getFieldSenderNameReturnsInitialValueForString()
    {
        self::assertSame(
            '',
            $this->subject->getFieldSenderName()
        );
    }

    /**
     * @test
     */
    public function setFieldSenderNameForStringSetsFieldSenderName()
    {
        $this->subject->setFieldSenderName('Conceived at T3CON10');

        self::assertAttributeEquals(
            'Conceived at T3CON10',
            'fieldSenderName',
            $this->subject
        );
    }

    /**
     * @test
     */
    public function getFieldSenderMailReturnsInitialValueForString()
    {
        self::assertSame(
            '',
            $this->subject->getFieldSenderMail()
        );
    }

    /**
     * @test
     */
    public function setFieldSenderMailForStringSetsFieldSenderMail()
    {
        $this->subject->setFieldSenderMail('Conceived at T3CON10');

        self::assertAttributeEquals(
            'Conceived at T3CON10',
            'fieldSenderMail',
            $this->subject
        );
    }

    /**
     * @test
     */
    public function getStoragePathReturnsInitialValueForString()
    {
        self::assertSame(
            '',
            $this->subject->getStoragePath()
        );
    }

    /**
     * @test
     */
    public function setStoragePathForStringSetsStoragePath()
    {
        $this->subject->setStoragePath('Conceived at T3CON10');

        self::assertAttributeEquals(
            'Conceived at T3CON10',
            'storagePath',
            $this->subject
        );
    }

    /**
     * @test
     */
    public function getStorageConfirmPathReturnsInitialValueForString()
    {
        self::assertSame(
            '',
            $this->subject->getStorageConfirmPath()
        );
    }

    /**
     * @test
     */
    public function setStorageConfirmPathForStringSetsStorageConfirmPath()
    {
        $this->subject->setStorageConfirmPath('Conceived at T3CON10');

        self::assertAttributeEquals(
            'Conceived at T3CON10',
            'storageConfirmPath',
            $this->subject
        );
    }

    /**
     * @test
     */
    public function getStepReturnsInitialValueForStep()
    {
        $newObjectStorage = new \TYPO3\CMS\Extbase\Persistence\ObjectStorage();
        self::assertEquals(
            $newObjectStorage,
            $this->subject->getStep()
        );
    }

    /**
     * @test
     */
    public function setStepForObjectStorageContainingStepSetsStep()
    {
        $step = new \KITT3N\Kitt3nForm\Domain\Model\Step();
        $objectStorageHoldingExactlyOneStep = new \TYPO3\CMS\Extbase\Persistence\ObjectStorage();
        $objectStorageHoldingExactlyOneStep->attach($step);
        $this->subject->setStep($objectStorageHoldingExactlyOneStep);

        self::assertAttributeEquals(
            $objectStorageHoldingExactlyOneStep,
            'step',
            $this->subject
        );
    }

    /**
     * @test
     */
    public function addStepToObjectStorageHoldingStep()
    {
        $step = new \KITT3N\Kitt3nForm\Domain\Model\Step();
        $stepObjectStorageMock = $this->getMockBuilder(\TYPO3\CMS\Extbase\Persistence\ObjectStorage::class)
            ->setMethods(['attach'])
            ->disableOriginalConstructor()
            ->getMock();

        $stepObjectStorageMock->expects(self::once())->method('attach')->with(self::equalTo($step));
        $this->inject($this->subject, 'step', $stepObjectStorageMock);

        $this->subject->addStep($step);
    }

    /**
     * @test
     */
    public function removeStepFromObjectStorageHoldingStep()
    {
        $step = new \KITT3N\Kitt3nForm\Domain\Model\Step();
        $stepObjectStorageMock = $this->getMockBuilder(\TYPO3\CMS\Extbase\Persistence\ObjectStorage::class)
            ->setMethods(['detach'])
            ->disableOriginalConstructor()
            ->getMock();

        $stepObjectStorageMock->expects(self::once())->method('detach')->with(self::equalTo($step));
        $this->inject($this->subject, 'step', $stepObjectStorageMock);

        $this->subject->removeStep($step);
    }

    /**
     * @test
     */
    public function getMailToSenderConfirmReturnsInitialValueForMailToSenderConfirm()
    {
        self::assertEquals(
            null,
            $this->subject->getMailToSenderConfirm()
        );
    }

    /**
     * @test
     */
    public function setMailToSenderConfirmForMailToSenderConfirmSetsMailToSenderConfirm()
    {
        $mailToSenderConfirmFixture = new \KITT3N\Kitt3nForm\Domain\Model\MailToSenderConfirm();
        $this->subject->setMailToSenderConfirm($mailToSenderConfirmFixture);

        self::assertAttributeEquals(
            $mailToSenderConfirmFixture,
            'mailToSenderConfirm',
            $this->subject
        );
    }

    /**
     * @test
     */
    public function getMailToSenderReturnsInitialValueForMailToSender()
    {
        self::assertEquals(
            null,
            $this->subject->getMailToSender()
        );
    }

    /**
     * @test
     */
    public function setMailToSenderForMailToSenderSetsMailToSender()
    {
        $mailToSenderFixture = new \KITT3N\Kitt3nForm\Domain\Model\MailToSender();
        $this->subject->setMailToSender($mailToSenderFixture);

        self::assertAttributeEquals(
            $mailToSenderFixture,
            'mailToSender',
            $this->subject
        );
    }

    /**
     * @test
     */
    public function getMailToReceiverReturnsInitialValueForMailToReceiver()
    {
        self::assertEquals(
            null,
            $this->subject->getMailToReceiver()
        );
    }

    /**
     * @test
     */
    public function setMailToReceiverForMailToReceiverSetsMailToReceiver()
    {
        $mailToReceiverFixture = new \KITT3N\Kitt3nForm\Domain\Model\MailToReceiver();
        $this->subject->setMailToReceiver($mailToReceiverFixture);

        self::assertAttributeEquals(
            $mailToReceiverFixture,
            'mailToReceiver',
            $this->subject
        );
    }
}
