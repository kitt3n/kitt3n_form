<?php
namespace KITT3N\Kitt3nForm\Tests\Unit\Controller;

/**
 * Test case.
 */
class RenderControllerTest extends \TYPO3\CMS\Core\Tests\UnitTestCase
{
    /**
     * @var \KITT3N\Kitt3nForm\Controller\RenderController
     */
    protected $subject = null;

    protected function setUp()
    {
        parent::setUp();
        $this->subject = $this->getMockBuilder(\KITT3N\Kitt3nForm\Controller\RenderController::class)
            ->setMethods(['redirect', 'forward', 'addFlashMessage'])
            ->disableOriginalConstructor()
            ->getMock();
    }

    protected function tearDown()
    {
        parent::tearDown();
    }

}
