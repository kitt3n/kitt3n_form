// var tx_kitt3nform__totalSize = 0;

var iPostMaxSize = 0;
var sPostMaxSizeIni = '';
var bPostMaxSizeError = false;
var bAnyError = false;
var iSumFileSize = 0;
var aTxKitt3nformBtnSubmit = document.getElementsByClassName("btn-submit");
var oTxKitt3nformFileinput = new Object();

function tx_kitt3nform__addClass(element, sClass) {
    var name, arr;
    name = sClass;
    arr = element.className.split(" ");
    if (arr.indexOf(name) == -1) {
        element.className += " " + name;
    }
}

function tx_kitt3nform__removeClass(element, sClass) {
    var sRegex = '\\b' + sClass + '\\b';
    var oRegex = new RegExp(sRegex,"g");
    element.className = element.className.replace(oRegex, "");
}

function tx_kitt3nform__insertAfter(newNode, referenceNode) {
    referenceNode.parentNode.insertBefore(newNode, referenceNode.nextSibling);
}

 var tx_kitt3nform__interval = setInterval(function () {
    if (typeof kitt3n_windowLoad == 'undefined') {
    } else {
        if (typeof kitt3n_windowLoad == "boolean" && kitt3n_windowLoad) {

            clearInterval(tx_kitt3nform__interval);

            var aTxKitt3nform = document.getElementsByClassName('tx_kitt3n-form__form');
            if (aTxKitt3nform.length > 0) {
                iPostMaxSize = aTxKitt3nform[0].getAttribute('data-post-max-size');
                sPostMaxSizeIni = aTxKitt3nform[0].getAttribute('data-post-max-size-ini');


                var aTxKitt3nformFileinput = document.getElementsByClassName("form-control-file");
                if (aTxKitt3nformFileinput.length > 0) {
                for (var ii = 0; ii < aTxKitt3nformFileinput.length; ii++) {

                    oTxKitt3nformFileinput[aTxKitt3nformFileinput[ii].getAttribute('id')] = new Object();
                    oTxKitt3nformFileinput[aTxKitt3nformFileinput[ii].getAttribute('id')]['size'] = 0;
                    oTxKitt3nformFileinput[aTxKitt3nformFileinput[ii].getAttribute('id')]['type'] = '';
                    oTxKitt3nformFileinput[aTxKitt3nformFileinput[ii].getAttribute('id')]['fileSizeAllowed'] = (aTxKitt3nformFileinput[ii].getAttribute('data-max-size') == null ? null : parseInt(aTxKitt3nformFileinput[ii].getAttribute('data-max-size')));
                    oTxKitt3nformFileinput[aTxKitt3nformFileinput[ii].getAttribute('id')]['fileSizeAllowedHumanReadable'] = aTxKitt3nformFileinput[ii].getAttribute('data-max-size-h');
                    oTxKitt3nformFileinput[aTxKitt3nformFileinput[ii].getAttribute('id')]['mimeTypesAllowed'] = aTxKitt3nformFileinput[ii].getAttribute('data-mime-types');
                    oTxKitt3nformFileinput[aTxKitt3nformFileinput[ii].getAttribute('id')]['fileSizeError'] = [false, tx_kitt3nform__lll_file_size.replace('{{ limit }} {{ suffix }}', aTxKitt3nformFileinput[ii].getAttribute('data-max-size-h'))];

                    var sEM = tx_kitt3nform__lll_file_mime_type.replace('{{ types }}', aTxKitt3nformFileinput[ii].getAttribute('data-mime-types'));
                    oTxKitt3nformFileinput[aTxKitt3nformFileinput[ii].getAttribute('id')]['mimeTypeError'] = [false, sEM];
                    oTxKitt3nformFileinput[aTxKitt3nformFileinput[ii].getAttribute('id')]['errorMessage'] = [];


                    aTxKitt3nformFileinput[ii].addEventListener('change', function(){
                        var file = this.files[0];
                        var inputId = this.getAttribute('id');
                        // This code is only for demo ...
                        console.log("name : " + file.name);
                        console.log("size : " + file.size);
                        console.log("type : " + file.type);
                        console.log("date : " + file.lastModified);

                        oTxKitt3nformFileinput[this.getAttribute('id')]['size'] = parseInt(file.size);
                        oTxKitt3nformFileinput[this.getAttribute('id')]['type'] = file.type;

                        /**
                         * Check actual file size against allowed file size
                         */
                        if ( ! isNaN(oTxKitt3nformFileinput[this.getAttribute('id')]['fileSizeAllowed'])
                            && oTxKitt3nformFileinput[this.getAttribute('id')]['fileSizeAllowed'] != null) {
                            if (oTxKitt3nformFileinput[this.getAttribute('id')]['size'] > oTxKitt3nformFileinput[this.getAttribute('id')]['fileSizeAllowed']) {
                                oTxKitt3nformFileinput[this.getAttribute('id')]['fileSizeError'][0] = true;
                            } else {
                                oTxKitt3nformFileinput[this.getAttribute('id')]['fileSizeError'][0] = false;
                            }
                        }

                        /**
                         * Check actual mimeType against allowed mimeTypes
                         */
                        if (oTxKitt3nformFileinput[this.getAttribute('id')]['mimeTypesAllowed'] != null) {
                            if (oTxKitt3nformFileinput[this.getAttribute('id')]['mimeTypesAllowed'].indexOf(oTxKitt3nformFileinput[this.getAttribute('id')]['type']) == -1) {
                                oTxKitt3nformFileinput[this.getAttribute('id')]['mimeTypeError'][0] = true;
                            } else {
                                oTxKitt3nformFileinput[this.getAttribute('id')]['mimeTypeError'][0] = false;
                            }
                        }

                        /**
                         * iSumFileSize
                         * @type {number}
                         */
                        iSumFileSize = 0;
                        for (var i = 0; i < Object.keys(oTxKitt3nformFileinput).length; i++) {
                            var sObjectKey = Object.keys(oTxKitt3nformFileinput)[i];
                            iSumFileSize += oTxKitt3nformFileinput[sObjectKey]['size'];
                        }

                        /**
                         * If size of all files is bigger then post_max_size
                         *  => set bPostMaxSizeError true
                         * else
                         *  => set bPostMaxSizeError false (default)
                         */
                        if (iSumFileSize > iPostMaxSize) {
                            bPostMaxSizeError = true;
                        } else {
                            bPostMaxSizeError = false;
                        }

                        /**
                         * Show or hide error messages below file(s)
                         */
                        bAnyError = false;
                        for (var i = 0; i < Object.keys(oTxKitt3nformFileinput).length; i++) {
                            var sObjectKey = Object.keys(oTxKitt3nformFileinput)[i];
                            var sInnerHtml = "";
                            if (oTxKitt3nformFileinput[sObjectKey]['fileSizeError'][0]) {
                                bAnyError = true;
                                sInnerHtml += '<div>' + oTxKitt3nformFileinput[sObjectKey]['fileSizeError'][1] + '</div>';
                            }
                            if (oTxKitt3nformFileinput[sObjectKey]['mimeTypeError'][0]) {
                                bAnyError = true;
                                sInnerHtml += '<div>' + oTxKitt3nformFileinput[sObjectKey]['mimeTypeError'][1].replace('{{ type }}', oTxKitt3nformFileinput[sObjectKey]['type']) + '</div>';
                            }
                            if (bPostMaxSizeError) {
                                bAnyError = true;
                                sInnerHtml += '<div>' + tx_kitt3nform__lll_post_max_size.replace('{{ post-max-size }}', sPostMaxSizeIni) + '</div>';
                            }
                            var el = document.getElementById(sObjectKey);
                            var elIF = document.getElementById('invalid-feedback-' + sObjectKey);
                            if (el != null && elIF != null) {
                                elIF.innerHTML = sInnerHtml;
                                if (sInnerHtml != '') {
                                    tx_kitt3nform__addClass(el, 'is-invalid');
                                } else {
                                    tx_kitt3nform__removeClass(el, 'is-invalid');
                                }
                            }
                        }
                        if (bAnyError) {
                            /**
                             * Add disabled state from submit button(s)
                             */
                            if (aTxKitt3nformBtnSubmit != null) {
                                for (var i = 0; i < aTxKitt3nformBtnSubmit.length; i++) {
                                    aTxKitt3nformBtnSubmit[i].setAttribute('disabled', 'disabled');
                                }
                            }
                        } else {
                            /**
                             * Remove disabled state from submit button(s)
                             */
                            if (aTxKitt3nformBtnSubmit != null) {
                                for (var i = 0; i < aTxKitt3nformBtnSubmit.length; i++) {
                                    aTxKitt3nformBtnSubmit[i].removeAttribute('disabled');
                                }
                            }
                        }

                    });

                }
            }

            }
            // document.getElementById('fileinput').addEventListener('change', function(){
            //     var file = this.files[0];
            //     // This code is only for demo ...
            //     console.log("name : " + file.name);
            //     console.log("size : " + file.size);
            //     console.log("type : " + file.type);
            //     console.log("date : " + file.lastModified);
            // }, false);
//
//             var tx_kitt3nform__fileinput = document.getElementsByClassName("form-control-file");
//             if (tx_kitt3nform__fileinput != null) {
//                 for (var i = 0; i < tx_kitt3nform__fileinput.length; i++) {
//                     tx_kitt3nform__totalSize = tx_kitt3nform__totalSize + tx_kitt3nform__fileinput[i].size;
//                 }
//                 console.log(tx_kitt3nform__totalSize);
//             }
        }
    }
});
// $("input:file").each(function(){
//     if($(this)[0].files[0]) {
//         totalSize = totalSize + $(this)[0].files[0].size;
//     }
// })