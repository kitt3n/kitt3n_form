<?php
defined('TYPO3_MODE') || die('Access denied.');

call_user_func(
    function()
    {

        \TYPO3\CMS\Extbase\Utility\ExtensionUtility::configurePlugin(
            'KITT3N.Kitt3nForm',
            'Kitt3nformformrender',
            [
                'Form' => 'render'
            ],
            // non-cacheable actions
            [
                'Form' => '',
                'Render' => ''
            ]
        );

        \TYPO3\CMS\Extbase\Utility\ExtensionUtility::configurePlugin(
            'KITT3N.Kitt3nForm',
            'Kitt3nformrenderrender',
            [
                'Render' => 'render'
            ],
            // non-cacheable actions
            [
                'Render' => 'render'
            ]
        );

    // wizards
    \TYPO3\CMS\Core\Utility\ExtensionManagementUtility::addPageTSConfig(
        'mod {
            wizards.newContentElement.wizardItems.plugins {
                elements {
                    kitt3nformformrender {
                        iconIdentifier = kitt3n_form-plugin-kitt3nformformrender
                        title = LLL:EXT:kitt3n_form/Resources/Private/Language/locallang_db.xlf:tx_kitt3n_form_kitt3nformformrender.name
                        description = LLL:EXT:kitt3n_form/Resources/Private/Language/locallang_db.xlf:tx_kitt3n_form_kitt3nformformrender.description
                        tt_content_defValues {
                            CType = list
                            list_type = kitt3nform_kitt3nformformrender
                        }
                    }
                    kitt3nformrenderrender {
                        iconIdentifier = kitt3n_form-plugin-kitt3nformrenderrender
                        title = LLL:EXT:kitt3n_form/Resources/Private/Language/locallang_db.xlf:tx_kitt3n_form_kitt3nformrenderrender.name
                        description = LLL:EXT:kitt3n_form/Resources/Private/Language/locallang_db.xlf:tx_kitt3n_form_kitt3nformrenderrender.description
                        tt_content_defValues {
                            CType = list
                            list_type = kitt3nform_kitt3nformrenderrender
                        }
                    }
                }
                show = *
            }
       }'
    );
		$iconRegistry = \TYPO3\CMS\Core\Utility\GeneralUtility::makeInstance(\TYPO3\CMS\Core\Imaging\IconRegistry::class);
		
			$iconRegistry->registerIcon(
				'kitt3n_form-plugin-kitt3nformformrender',
				\TYPO3\CMS\Core\Imaging\IconProvider\SvgIconProvider::class,
				['source' => 'EXT:kitt3n_form/Resources/Public/Icons/user_plugin_kitt3nformformrender.svg']
			);
		
			$iconRegistry->registerIcon(
				'kitt3n_form-plugin-kitt3nformrenderrender',
				\TYPO3\CMS\Core\Imaging\IconProvider\SvgIconProvider::class,
				['source' => 'EXT:kitt3n_form/Resources/Public/Icons/user_plugin_kitt3nformrenderrender.svg']
			);
		
    }
);
## EXTENSION BUILDER DEFAULTS END TOKEN - Everything BEFORE this line is overwritten with the defaults of the extension builder

call_user_func(
    function($extKey)
    {
        \TYPO3\CMS\Core\Utility\ExtensionManagementUtility::addPageTSConfig(
            'mod {
                wizards.newContentElement.wizardItems.plugins.elements.kitt3nformformrender >
                wizards.newContentElement.wizardItems.plugins.elements.kitt3nformrenderrender >
                wizards.newContentElement.wizardItems {
                    KITT3N {
                        header = KITT3N
                        after = common,special,menu,plugins,forms
                        elements.kitt3nformrenderrender {
                            iconIdentifier = kitt3n_svg_big
                            title = kitt3n | Form
                            description = Divine form
                            tt_content_defValues {
                                CType = list
                                list_type = kitt3nform_kitt3nformrenderrender
                            }
                        }
                        show := addToList(kitt3nformrenderrender)
                    }

                }
            }'
        );

    }, $_EXTKEY
);


/*
 * Hooks (useless in TYPO3 9.5.x)
 */
if (\TYPO3\CMS\Core\Utility\ExtensionManagementUtility::isLoaded('realurl')) {
    $GLOBALS['TYPO3_CONF_VARS']['SC_OPTIONS']['ext/realurl/class.tx_realurl_autoconfgen.php']['extensionConfiguration']['kitt3n_form'] =
        \KITT3N\Kitt3nForm\Hooks\RealUrlAutoConfiguration::class . '->addConfig';

    $GLOBALS['TYPO3_CONF_VARS']['SC_OPTIONS']['ext/realurl/class.tx_realurl_autoconfgen.php']['postProcessConfiguration']['kitt3n_form'] =
        \KITT3N\Kitt3nForm\Hooks\RealUrlAutoConfiguration::class . '->postProcessConfiguration';
}

/*
 * Example Signals
 *
 * Use in own extension!
 */
///** @var \TYPO3\CMS\Extbase\SignalSlot\Dispatcher $signalSlotDispatcher */
//$signalSlotDispatcher = \TYPO3\CMS\Core\Utility\GeneralUtility::makeInstance(\TYPO3\CMS\Extbase\SignalSlot\Dispatcher::class);
//
//$signalSlotDispatcher->connect(
//    \KITT3N\Kitt3nForm\Controller\RenderController::class,
//    'addFieldToFormOverrideReadonlyType',
//    \KITT3N\Kitt3nForm\Slot\RenderControllerSlot::class,
//    'addFieldToFormOverrideReadonlyType',
//    FALSE
//);

///** @var \TYPO3\CMS\Extbase\SignalSlot\Dispatcher $signalSlotDispatcher */
//$signalSlotDispatcher1 = \TYPO3\CMS\Core\Utility\GeneralUtility::makeInstance(\TYPO3\CMS\Extbase\SignalSlot\Dispatcher::class);
//
//$signalSlotDispatcher1->connect(
//    \KITT3N\Kitt3nForm\Controller\RenderController::class,
//    'addFieldToFormOverrideHiddenType',
//    \KITT3N\Kitt3nForm\Slot\RenderControllerSlot::class,
//    'addFieldToFormOverrideHiddenType',
//    FALSE
//);