<?php

namespace KITT3N\Kitt3nForm\Service;

/***
 *
 * This file is part of the "kitt3n_form" Extension for TYPO3 CMS.
 *
 * For the full copyright and license information, please read the
 * LICENSE.txt file that was distributed with this source code.
 *
 *  (c) 2018
 *
 ***/

/**
 * Class UnitService
 * @package KITT3N\Kitt3nForm\Service
 */
class UnitService implements \TYPO3\CMS\Core\SingletonInterface
{

    public function getMultipliersForUnits()
    {
        $aMultiplyByUnit = [
            "G" => intval(1024 * 1024 * 1024),
            "M" => intval(1024 * 1024),
            "K" => intval(1024),
        ];

        return $aMultiplyByUnit;
    }

    public function getMultiplierForGivenUnit($sUnit = '')
    {
        $aMultiplyByUnit = $this->getMultipliersForUnits();

        return $aMultiplyByUnit[$sUnit];
    }

    public function getSizeInBytes($sFileSize = '')
    {
        $fFileSize = floatval(str_replace(['G', 'M', 'K'], ['', '', ''], $sFileSize));
        $sFileSizeSuffix = (strpos($sFileSize, 'G') !== false ? "G" : (strpos($sFileSize, 'M') !== false ? "M" : "K"));
        $iFileSizeInBytes = intval($fFileSize * $this->getMultiplierForGivenUnit($sFileSizeSuffix));

        return $iFileSizeInBytes;
    }
}