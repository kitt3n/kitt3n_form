<?php

namespace KITT3N\Kitt3nForm\Service;

use ReflectionClass;

/***
 *
 * This file is part of the "kitt3n_form" Extension for TYPO3 CMS.
 *
 * For the full copyright and license information, please read the
 * LICENSE.txt file that was distributed with this source code.
 *
 *  (c) 2018
 *
 ***/

/**
 * Class UPatternService
 * @package KITT3N\Kitt3nForm\Service
 */
class PatternService implements \TYPO3\CMS\Core\SingletonInterface
{
    /**
     *
     */
    const EXTENSION_KEY = 'kitt3n_form';

    /**
     *
     */
    const DEFAULT_TRANSLATION_PATH = 'LLL:EXT:kitt3n_form/Resources/Private/Language/translation.xlf:';

    /**
     *
     */
    const VALIDATOR_TRANSLATION_PATH = 'LLL:EXT:kitt3n_form/Resources/Private/Language/vendor/symfony/validator/Resources/translations/validators.xlf:';

    const FIELD__MANDATORY = '<span class="mandatory">&nbsp;*&nbsp;</span>';

    /**
     *
     */
    const PROCESS_NUMBER__SEPARATOR = "_";

    const PROCESS_NUMBER_PLACEHOLDER__PAGE_UID = "{{{iPageUid}}}";

    const PROCESS_NUMBER_PLACEHOLDER__ELEMENT_UID = "{{{iElementUid}}}";

    const PROCESS_NUMBER_PLACEHOLDER__FORM_UID = "{{{iFormUid}}}";

    /**
     *
     */
    const FORM__SEPARATOR = "_";

    const FORM_PLACEHOLDER__PREFIX = ['p', 'e', 'f', 's'];

    const FORM_PLACEHOLDER__PAGE_UID = "p{{{iPageUid}}}";

    const FORM_PLACEHOLDER__ELEMENT_UID = "e{{{iElementUid}}}";

    const FORM_PLACEHOLDER__FORM_UID = "f{{{iFormUid}}}";

    const FORM_PLACEHOLDER__STEP_UID = "s{{{iStepUid}}}";

    /**
     *
     */
    const CONTROL_FIELD_PREFIX = "__";

    /**
     *
     */
    const HELPER_FIELD_SUFFIX = "--";

    /**
     *
     */
    const FIELD__SEPARATOR = "_";

    const FIELD_PLACEHOLDER__PAGE_UID = "{{{iPageUid}}}";

    const FIELD_PLACEHOLDER__ELEMENT_UID = "{{{iElementUid}}}";

    const FIELD_PLACEHOLDER__FORM_UID = "{{{iFormUid}}}";

    const FIELD_PLACEHOLDER__STEP_UID = "{{{iStepUid}}}";

    const FIELD_PLACEHOLDER__FIELDSET_UID = "{{{iFieldsetUid}}}";

    const FIELD_PLACEHOLDER__FIELD_UID = "{{{iFieldUid}}}";

    /**
     * @return string
     */
    public function getProcessNumberPattern()
    {
        return self::PROCESS_NUMBER_PLACEHOLDER__FORM_UID;
    }

    public function getProcessNumberPatternArray()
    {
        return array_flip(explode(self::PROCESS_NUMBER__SEPARATOR, $this->getProcessNumberPattern()));
    }

    public function getValueFromProcessNumberNameByPlaceholder($sProcessNumberName = '', $sPlaceholder = '')
    {
        $aProcessNumberName = explode(self::PROCESS_NUMBER__SEPARATOR, $sProcessNumberName);
        $aProcessNumberPatternArray = $this->getProcessNumberPatternArray();
        if (array_key_exists($sPlaceholder, $aProcessNumberPatternArray)) {
            return ($aProcessNumberPatternArray[$sPlaceholder] < count($aProcessNumberName) ? $aProcessNumberName[$aProcessNumberPatternArray[$sPlaceholder]] : null);
        } else {
            return null;
        }
    }

    /**
     * @return string
     */
    public function getFormPattern()
    {
        return self::FORM_PLACEHOLDER__FORM_UID . self::FORM__SEPARATOR .
               self::FORM_PLACEHOLDER__STEP_UID;
    }

    public function getFormPatternArray()
    {
        return array_flip(explode(self::FORM__SEPARATOR, $this->getFormPattern()));
    }

    public function getValueFromFormNameByPlaceholder($oFormViewVarsName = '', $sPlaceholder = '')
    {
        $aFormViewVarsName = explode(self::FORM__SEPARATOR, $oFormViewVarsName);
        $aFormPatternArray = $this->getFormPatternArray();
        if (array_key_exists($sPlaceholder, $aFormPatternArray)) {
            return ($aFormPatternArray[$sPlaceholder] < count($aFormViewVarsName) ? intval(str_replace(self::FORM_PLACEHOLDER__PREFIX, ['',''], $aFormViewVarsName[$aFormPatternArray[$sPlaceholder]])) : null);
        } else {
            return null;
        }
    }

    /**
     * @return string
     */
    public function getFieldPattern()
    {
        return self::FIELD_PLACEHOLDER__FORM_UID . self::FIELD__SEPARATOR .
               self::FIELD_PLACEHOLDER__STEP_UID . self::FIELD__SEPARATOR .
               self::FIELD_PLACEHOLDER__FIELDSET_UID . self::FIELD__SEPARATOR .
               self::FIELD_PLACEHOLDER__FIELD_UID;
    }

    public function getFieldPatternArray()
    {
        return array_flip(explode(self::FIELD__SEPARATOR, $this->getFieldPattern()));
    }

    public function getValueFromFieldNameByPlaceholder($oFormFormViewVarsName = '', $sPlaceholder = '')
    {
        $aFormFormViewVarsName = explode(self::FIELD__SEPARATOR, $oFormFormViewVarsName);
        $aFieldPatternArray = $this->getFieldPatternArray();
        if (array_key_exists($sPlaceholder, $aFieldPatternArray)) {
            return ($aFieldPatternArray[$sPlaceholder] < count($aFormFormViewVarsName) ? $aFormFormViewVarsName[$aFieldPatternArray[$sPlaceholder]] : null);
        } else {
            return null;
        }
    }

    /**
     * @return array
     */
    public function getConstArray()
    {
        $oClass = new ReflectionClass('\\KITT3N\\Kitt3nForm\\Service\\PatternService');
        $aConst = $oClass->getConstants();

        return $aConst;
    }
}