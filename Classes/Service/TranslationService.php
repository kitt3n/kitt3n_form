<?php

namespace KITT3N\Kitt3nForm\Service;

use TYPO3\CMS\Core\Utility\GeneralUtility;

/***
 *
 * This file is part of the "kitt3n_form" Extension for TYPO3 CMS.
 *
 * For the full copyright and license information, please read the
 * LICENSE.txt file that was distributed with this source code.
 *
 *  (c) 2018
 *
 ***/

/**
 * Class TranslationService
 * @package KITT3N\Kitt3nForm\Service
 *
 * TODO Understand how Symfony handles translations and change our shitty translation handling
 */
class TranslationService implements \TYPO3\CMS\Core\SingletonInterface
{
    const EXTENSION_KEY = 'kitt3n_form';

    const DEFAULT_TRANSLATION_PATH = 'LLL:EXT:kitt3n_form/Resources/Private/Language/translation.xlf:';

    const VALIDATOR_TRANSLATION_PATH = 'LLL:EXT:kitt3n_form/Resources/Private/Language/vendor/symfony/validator/Resources/translations/validators.xlf:';

    public function getLocalizedErrorMessageByErrorcode($sErrorcode): string
    {

        /*
         * bd79c0ab-ddba-46cc-a703-a7a4b08de310 => Assert\Email()
         * 9ff3fdc4-b214-49db-8718-39c315e33d45 => Assert\Length() (min x)
         * ad32d13f-c3d4-423b-909a-857b961eb720 => Assert\NotNull() (not empty)
         * 2beabf1c-54c0-4882-a928-05249b26e23b => Assert\IsTrue()
         *
         * Custom Errors
         *
         * file-mime-type
         * file-size
         * file-size-sum
         */

        $aErrorMessage = [
            'bd79c0ab-ddba-46cc-a703-a7a4b08de310' => \TYPO3\CMS\Extbase\Utility\LocalizationUtility::translate(
                self::VALIDATOR_TRANSLATION_PATH . 13, self::EXTENSION_KEY
            ),
            '9ff3fdc4-b214-49db-8718-39c315e33d45' => \TYPO3\CMS\Extbase\Utility\LocalizationUtility::translate(
                self::VALIDATOR_TRANSLATION_PATH . 21, self::EXTENSION_KEY
            ),
            'ad32d13f-c3d4-423b-909a-857b961eb720' => \TYPO3\CMS\Extbase\Utility\LocalizationUtility::translate(
                self::VALIDATOR_TRANSLATION_PATH . 22, self::EXTENSION_KEY
            ),
            '2beabf1c-54c0-4882-a928-05249b26e23b' => \TYPO3\CMS\Extbase\Utility\LocalizationUtility::translate(
                self::VALIDATOR_TRANSLATION_PATH . 22, self::EXTENSION_KEY
            ),
            'c1051bb4-d103-4f74-8988-acbcafc7fdc3' => \TYPO3\CMS\Extbase\Utility\LocalizationUtility::translate(
                self::VALIDATOR_TRANSLATION_PATH . 22, self::EXTENSION_KEY
            ),

            'file-mime-type' => \TYPO3\CMS\Extbase\Utility\LocalizationUtility::translate(
                self::VALIDATOR_TRANSLATION_PATH . 17, self::EXTENSION_KEY
            ),

            'file-size' => \TYPO3\CMS\Extbase\Utility\LocalizationUtility::translate(
                self::VALIDATOR_TRANSLATION_PATH . 32, self::EXTENSION_KEY
            ),

            'file-size-sum' => \TYPO3\CMS\Extbase\Utility\LocalizationUtility::translate(
                self::VALIDATOR_TRANSLATION_PATH . 50, self::EXTENSION_KEY
            ),

            'post-max-size' => \TYPO3\CMS\Extbase\Utility\LocalizationUtility::translate(
                self::DEFAULT_TRANSLATION_PATH . 'tx_kitt3nform.errormessage.post-max-size', self::EXTENSION_KEY
            ),
        ];

        /*
         * TODO Slot
         */

        if ( ! array_key_exists($sErrorcode, $aErrorMessage)) {
            $sErrorMessage = $sErrorcode;

            return $sErrorMessage;
        }

        /*
         * Error messages sometimes contain a delimiter "|". The error messages before and after this delimiter are equal to each other.
         * I have no idea why!
         */

        $sErrorMessage = explode('|', $aErrorMessage[$sErrorcode])[0];

        return $sErrorMessage;

    }

    /*
     * Error messages must be translated!
     *
     * $formView gives us only english (en) error messages.
     * We have to do some crazy shit to translate the messages below ;).
     *
     *
     * @param \Symfony\Component\Form\FormError $oFormError
     */
    public function getErrorMessageForFormError($oFormError)
    {
        $sErrorMessage = "";

        /*
         * Get the Cause object
         *
         * @var \Symfony\Component\Validator\ConstraintViolation $oCause
         */

        $oCause = $oFormError->getCause();
        /*
         * Get the error code from Cause object.
         * Use this error code to get the corresponding error message from $aErrorMessage.
         *
         * @var string $sCauseCode
         */

        $sCauseCode = $oCause->getCode();
        /*
         * Error messages sometimes contain a delimiter "|". The error messages before and after this delimiter are equal to each other.
         * I have no idea why!
         */

        $sErrorMessage = $this->getLocalizedErrorMessageByErrorcode($sCauseCode);
        /*
         * Error message sometimes contains twig placeholders like "{{ limit }}" {{ suffix }} etc.
         * Placeholders must be replaced before rendering the error message in frontend.
         *
         * @var array $aCauseParameters
         */

        $aCauseParameters = $oCause->getParameters();
        if (count($aCauseParameters) > 0) {
            foreach ($aCauseParameters as $sCauseKey => $sCauseParameter) {
                $sErrorMessage = $sCauseKey != '{{ value }}' ? str_replace($sCauseKey,
                    $sCauseParameter, $sErrorMessage
                ) : $sErrorMessage;
            }
        }

        return $sErrorMessage;
    }

    public function getLocalizationsForJavaScript()
    {

        $aErrorcode = [
            'file-mime-type',
            'file-size',
            'file-size-sum',
            'post-max-size',
        ];

        $sLocalizationsForJavaScript = '<script type="text/javascript">';

        foreach ($aErrorcode as $sErrorcode) {
            $sLocalizationsForJavaScript .= 'var tx_kitt3nform__lll_' . str_replace("-", "_",
                    $sErrorcode) . ' = "' . $this->getLocalizedErrorMessageByErrorcode($sErrorcode) . '";';
        }

        $sLocalizationsForJavaScript .= '</script>';

        return $sLocalizationsForJavaScript;

    }
}