<?php
namespace KITT3N\Kitt3nForm\Hooks;
/**
 * This file is part of the "kitt3n_form" Extension for TYPO3 CMS.
 *
 * For the full copyright and license information, please read the
 * LICENSE.txt file that was distributed with this source code.
 */

/**
 * AutoConfiguration-Hook for RealURL
 *
 */
class RealUrlAutoConfiguration
{
    /**
     * Generates additional RealURL configuration and merges it with provided configuration
     *
     * @param       array $params Default configuration
     * @return      array Updated configuration
     */
    public function addConfig($params)
    {
        return array_merge_recursive(
            $params['config'],
            [
                'lastGenerated' => time()
            ]
        );
    }

    /**
     * @param array $configuration
     */
    public function postProcessConfiguration(array &$parameters) {
        $parameters['config']['_DEFAULT']['postVarSets']['_DEFAULT']['kitt3n_form'] = [
            [
                'GETvar' => 'tx_kitt3nform_kitt3nformrenderrender[action]',
                'noMatch' => 'bypass',
            ],
            [
                'GETvar' => 'tx_kitt3nform_kitt3nformrenderrender[controller]',
                'noMatch' => 'bypass',
            ],
            [
                'GETvar' => 'tx_kitt3nform_kitt3nformrenderrender[token]',
            ],
            [
                'GETvar' => 'tx_kitt3nform_kitt3nformrenderrender[token1]',
            ],
            [
                'GETvar' => 'tx_kitt3nform_kitt3nformrenderrender[element]',
            ],
        ];

//        $parameters['config']['_DOMAINS'] = [
//            'encode' => [
//                [
//                    'GETvar' => 'L',
//                    'value' => '0',
//                    'urlPrepend' => 'http://development.localhost',
//                    'useConfiguration' => 'development.localhost',
//                ],
//                [
//                    'GETvar' => 'L',
//                    'value' => '1',
//                    'urlPrepend' => 'http://development.localhost.de',
//                    'useConfiguration' => 'development.localhost.de',
//                 ],
//                [
//                    'GETvar' => 'L',
//                    'value' => '2',
//                    'urlPrepend' => 'http://development.localhost.ch',
//                    'useConfiguration' => 'development.localhost.ch',
//                ],
//            ],
//            'decode' => [
//                'development.localhost.com' => [
//                    'GETvars' => [
//                        'L' => 0,
//                    ],
//                    'useConfiguration' => 'development.localhost',
//                ],
//                'development.localhost.de' => [
//                    'GETvars' => [
//                        'L' => 1,
//                    ],
//                    'useConfiguration' => 'development.localhost.de',
//                ],
//                'development.localhost.ch' => [
//                    'GETvars' => [
//                        'L' => 2,
//                    ],
//                    'useConfiguration' => 'development.localhost.ch',
//                ],
//            ],
//        ];
    }
}