<?php

namespace KITT3N\Kitt3nForm\Slot;

/***
 *
 * This file is part of the "kitt3n_form" Extension for TYPO3 CMS.
 *
 * For the full copyright and license information, please read the
 * LICENSE.txt file that was distributed with this source code.
 *
 *  (c) 2018
 *
 ***/

/**
 * Class SessionService
 * @package KITT3N\Kitt3nForm\Slot
 */
class RenderControllerSlot
{

    /**
     * Use a slot like this to override a specific ReadonlyField
     * Please be aware that ExtensionBuilder removes the VERY IMPORTANT & in parameters
     *
     * @param $sKey
     * @param $aAttr
     * @param $sLabel
     */
    public function addFieldToFormOverrideReadonlyType(
        $sKey,
        &$aAttr,
        &$sLabel
    )
    {
        switch ($sKey) {
            case '1_38_1_1_1_12':
                // please us a existing $sKey from a ReadonlyType field in your form ;)
                // <page_uid>_<plugin_uid>_<form_uid>_<step_uid>_<fieldset_uid>_<field_uid>
//                $aAttr['value'] = 'Maschinenbauer (m/w)';
//                $sLabel = 'Ihre Bewerbung für die Stelle als';
                break;
            default:
        }
    }

    /**
     * Use a slot like this to override a specific HiddenField
     * Please be aware that ExtensionBuilder removes the VERY IMPORTANT & in parameters
     *
     * @param $sKey
     * @param $aAttr
     * @param $sLabel
     */
    public function addFieldToFormOverrideHiddenType(
        $sKey,
        &$aAttr,
        &$sLabel
    )
    {
        switch ($sKey) {
            case '1_38_1_1_1_12':
                // please us a existing $sKey from a HiddenType field in your form ;)
                // <page_uid>_<plugin_uid>_<form_uid>_<step_uid>_<fieldset_uid>_<field_uid>
//                $aAttr['value'] = 'Maschinenbauer (m/w)';
//                $sLabel = 'Ihre Bewerbung für die Stelle als';
                break;
            default:
        }
    }
}