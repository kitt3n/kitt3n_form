
plugin.tx_kitt3nform_kitt3nformformrender {
    view {
        # cat=plugin.tx_kitt3nform_kitt3nformformrender/file; type=string; label=Path to template root (FE)
        templateRootPath = EXT:kitt3n_form/Resources/Private/Templates/
        # cat=plugin.tx_kitt3nform_kitt3nformformrender/file; type=string; label=Path to template partials (FE)
        partialRootPath = EXT:kitt3n_form/Resources/Private/Partials/
        # cat=plugin.tx_kitt3nform_kitt3nformformrender/file; type=string; label=Path to template layouts (FE)
        layoutRootPath = EXT:kitt3n_form/Resources/Private/Layouts/
    }
    persistence {
        # cat=plugin.tx_kitt3nform_kitt3nformformrender//a; type=string; label=Default storage PID
        storagePid =
    }
}

plugin.tx_kitt3nform_kitt3nformrenderrender {
    view {
        # cat=plugin.tx_kitt3nform_kitt3nformrenderrender/file; type=string; label=Path to template root (FE)
        templateRootPath = EXT:kitt3n_form/Resources/Private/Templates/
        # cat=plugin.tx_kitt3nform_kitt3nformrenderrender/file; type=string; label=Path to template partials (FE)
        partialRootPath = EXT:kitt3n_form/Resources/Private/Partials/
        # cat=plugin.tx_kitt3nform_kitt3nformrenderrender/file; type=string; label=Path to template layouts (FE)
        layoutRootPath = EXT:kitt3n_form/Resources/Private/Layouts/
    }
    persistence {
        # cat=plugin.tx_kitt3nform_kitt3nformrenderrender//a; type=string; label=Default storage PID
        storagePid =
    }
}

## EXTENSION BUILDER DEFAULTS END TOKEN - Everything BEFORE this line is overwritten with the defaults of the extension builder

plugin {
    tx_kitt3nform {
        model {
            KITT3N\Kitt3nForm\Domain\Model\Render {
                persistence {
                    storagePid =
                }
            }
            KITT3N\Kitt3nForm\Domain\Model\Form {
                persistence {
                    storagePid =
                }
            }
            KITT3N\Kitt3nForm\Domain\Model\Step {
                persistence {
                    storagePid =
                }
            }
            KITT3N\Kitt3nForm\Domain\Model\Fieldset {
                persistence {
                    storagePid =
                }
            }
            KITT3N\Kitt3nForm\Domain\Model\Field {
                persistence {
                    storagePid =
                }
            }
            KITT3N\Kitt3nForm\Domain\Model\Constraint {
                persistence {
                    storagePid =
                }
            }
            KITT3N\Kitt3nForm\Domain\Model\MailToSenderConfirm {
                persistence {
                    storagePid =
                }
            }
            KITT3N\Kitt3nForm\Domain\Model\MailToSender {
                persistence {
                    storagePid =
                }
            }
            KITT3N\Kitt3nForm\Domain\Model\MailToReceiver {
                persistence {
                    storagePid =
                }
            }
        }

        settings {
            mail {
                to {
                    sender {
                        enable = 1
                        from {
                            name =
                            mail =
                            subject =
                        }
                        replyTo {
                            name =
                            mail =
                        }
                    }
                    receiver {
                        enable = 1
                        from {
                            name =
                            mail =
                            subject =
                        }
                        to {
                            name =
                            mail =
                            subject =
                            cc =
                            bcc =
                        }
                    }
                }
            }

            storage {
                enable = 1
                ## Ansolute from webroot
                absolute_path_from_webroot = /fileadmin/kitt3n_form/default
                confirm {
                    ## Ansolute from webroot
                    absolute_path_from_webroot = /fileadmin/user_upload/kitt3n_form/default
                }
            }

            validation {
                captcha {
                    reCaptcha {
                        sitekey =
                        secret =
                    }
                    zwei14 = 0
                }
            }
        }
    }
}