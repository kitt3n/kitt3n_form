
plugin.tx_kitt3nform_kitt3nformformrender {
    view {
        templateRootPaths.0 = EXT:{extension.extensionKey}/Resources/Private/Templates/
        templateRootPaths.1 = {$plugin.tx_kitt3nform_kitt3nformformrender.view.templateRootPath}
        partialRootPaths.0 = EXT:kitt3n_form/Resources/Private/Partials/
        partialRootPaths.1 = {$plugin.tx_kitt3nform_kitt3nformformrender.view.partialRootPath}
        layoutRootPaths.0 = EXT:kitt3n_form/Resources/Private/Layouts/
        layoutRootPaths.1 = {$plugin.tx_kitt3nform_kitt3nformformrender.view.layoutRootPath}
    }
    persistence {
        storagePid = {$plugin.tx_kitt3nform_kitt3nformformrender.persistence.storagePid}
        #recursive = 1
    }
    features {
        #skipDefaultArguments = 1
        # if set to 1, the enable fields are ignored in BE context
        ignoreAllEnableFieldsInBe = 0
        # Should be on by default, but can be disabled if all action in the plugin are uncached
        requireCHashArgumentForActionArguments = 1
    }
    mvc {
        #callDefaultActionIfActionCantBeResolved = 1
    }
}

plugin.tx_kitt3nform_kitt3nformrenderrender {
    view {
        templateRootPaths.0 = EXT:{extension.extensionKey}/Resources/Private/Templates/
        templateRootPaths.1 = {$plugin.tx_kitt3nform_kitt3nformrenderrender.view.templateRootPath}
        partialRootPaths.0 = EXT:kitt3n_form/Resources/Private/Partials/
        partialRootPaths.1 = {$plugin.tx_kitt3nform_kitt3nformrenderrender.view.partialRootPath}
        layoutRootPaths.0 = EXT:kitt3n_form/Resources/Private/Layouts/
        layoutRootPaths.1 = {$plugin.tx_kitt3nform_kitt3nformrenderrender.view.layoutRootPath}
    }
    persistence {
        storagePid = {$plugin.tx_kitt3nform_kitt3nformrenderrender.persistence.storagePid}
        #recursive = 1
    }
    features {
        #skipDefaultArguments = 1
        # if set to 1, the enable fields are ignored in BE context
        ignoreAllEnableFieldsInBe = 0
        # Should be on by default, but can be disabled if all action in the plugin are uncached
        requireCHashArgumentForActionArguments = 1
    }
    mvc {
        #callDefaultActionIfActionCantBeResolved = 1
    }
}

# these classes are only used in auto-generated templates
plugin.tx_kitt3nform._CSS_DEFAULT_STYLE (
    textarea.f3-form-error {
        background-color:#FF9F9F;
        border: 1px #FF0000 solid;
    }

    input.f3-form-error {
        background-color:#FF9F9F;
        border: 1px #FF0000 solid;
    }

    .tx-kitt3n-form table {
        border-collapse:separate;
        border-spacing:10px;
    }

    .tx-kitt3n-form table th {
        font-weight:bold;
    }

    .tx-kitt3n-form table td {
        vertical-align:top;
    }

    .typo3-messages .message-error {
        color:red;
    }

    .typo3-messages .message-ok {
        color:green;
    }
)

## EXTENSION BUILDER DEFAULTS END TOKEN - Everything BEFORE this line is overwritten with the defaults of the extension builder

plugin {
    tx_kitt3nform {
        model {
            KITT3N\Kitt3nForm\Domain\Model\Render {
                persistence {
                    storagePid = {$plugin.tx_kitt3nform.model.KITT3N\Kitt3nForm\Domain\Model\Render.persistence.storagePid}
                }
            }
            KITT3N\Kitt3nForm\Domain\Model\Form {
                persistence {
                    storagePid = {$plugin.tx_kitt3nform.model.KITT3N\Kitt3nForm\Domain\Model\Form.persistence.storagePid}
                }
            }
            KITT3N\Kitt3nForm\Domain\Model\Step {
                persistence {
                    storagePid = {$plugin.tx_kitt3nform.model.KITT3N\Kitt3nForm\Domain\Model\Step.persistence.storagePid}
                }
            }
            KITT3N\Kitt3nForm\Domain\Model\Fieldset {
                persistence {
                    storagePid = {$plugin.tx_kitt3nform.model.KITT3N\Kitt3nForm\Domain\Model\Fieldset.persistence.storagePid}
                }
            }
            KITT3N\Kitt3nForm\Domain\Model\Field {
                persistence {
                    storagePid = {$plugin.tx_kitt3nform.model.KITT3N\Kitt3nForm\Domain\Model\Field.persistence.storagePid}
                }
            }
            KITT3N\Kitt3nForm\Domain\Model\Constraint {
                persistence {
                    storagePid = {$plugin.tx_kitt3nform.model.KITT3N\Kitt3nForm\Domain\Model\Constraint.persistence.storagePid}
                }
            }
            KITT3N\Kitt3nForm\Domain\Model\MailToSenderConfirm {
                persistence {
                    storagePid = {$plugin.tx_kitt3nform.model.KITT3N\Kitt3nForm\Domain\Model\MailToSenderConfirm.persistence.storagePid}
                }
            }
            KITT3N\Kitt3nForm\Domain\Model\MailToSender {
                persistence {
                    storagePid = {$plugin.tx_kitt3nform.model.KITT3N\Kitt3nForm\Domain\Model\MailToSender.persistence.storagePid}
                }
            }
            KITT3N\Kitt3nForm\Domain\Model\MailToReceiver {
                persistence {
                    storagePid = {$plugin.tx_kitt3nform.model.KITT3N\Kitt3nForm\Domain\Model\MailToReceiver.persistence.storagePid}
                }
            }
        }

        settings {
            mail {
                to {
                    sender {
                        enable = {$plugin.tx_kitt3nform.settings.mail.to.sender.enable}
                        from {
                            name = {$plugin.tx_kitt3nform.settings.mail.to.sender.from.name}
                            mail = {$plugin.tx_kitt3nform.settings.mail.to.sender.from.mail}
                            subject = {$plugin.tx_kitt3nform.settings.mail.to.sender.from.subject}
                        }
                        replyTo {
                            name = {$plugin.tx_kitt3nform.settings.mail.to.sender.replyTo.name}
                            mail = {$plugin.tx_kitt3nform.settings.mail.to.sender.replyTo.mail}
                        }
                        format {
                            html = 0
                            plain = 1
                        }
                    }
                    receiver {
                        enable = {$plugin.tx_kitt3nform.settings.mail.to.receiver.enable}
                        from {
                            name = {$plugin.tx_kitt3nform.settings.mail.to.receiver.from.name}
                            mail = {$plugin.tx_kitt3nform.settings.mail.to.receiver.from.mail}
                            subject = {$plugin.tx_kitt3nform.settings.mail.to.receiver.from.subject}
                        }
                        to {
                            name = {$plugin.tx_kitt3nform.settings.mail.to.receiver.to.name}
                            mail = {$plugin.tx_kitt3nform.settings.mail.to.receiver.to.mail}
                            subject = {$plugin.tx_kitt3nform.settings.mail.to.receiver.to.subject}
                            cc = {$plugin.tx_kitt3nform.settings.mail.to.receiver.to.cc}
                            bcc = {$plugin.tx_kitt3nform.settings.mail.to.receiver.to.bcc}
                        }
                        format {
                            html = 0
                            plain = 1
                        }
                    }
                }
            }

            storage {
                enable = {$plugin.tx_kitt3nform.settings.storage.enable}
                absolute_path_from_webroot = {$plugin.tx_kitt3nform.settings.storage.absolute_path_from_webroot}
                confirm {
                    absolute_path_from_webroot = {$plugin.tx_kitt3nform.settings.storage.confirm.absolute_path_from_webroot}
                }
            }

            validation {
                captcha {
                    reCaptcha{
                        sitekey = {$plugin.tx_kitt3nform.settings.validation.captcha.reCaptcha.sitekey}
                        secret = {$plugin.tx_kitt3nform.settings.validation.captcha.reCaptcha.secret}
                    }
                    zwei14 = {$plugin.tx_kitt3nform.settings.validation.captcha.zwei14}
                }
            }

        }

    }
}