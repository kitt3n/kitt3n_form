<?php
return [
    'ctrl' => [
        'title' => 'LLL:EXT:kitt3n_form/Resources/Private/Language/locallang_db.xlf:tx_kitt3nform_domain_model_mailtosender',
        'label' => 'title',
        'tstamp' => 'tstamp',
        'crdate' => 'crdate',
        'cruser_id' => 'cruser_id',
        'sortby' => 'sorting',
        'versioningWS' => true,
        'languageField' => 'sys_language_uid',
        'transOrigPointerField' => 'l10n_parent',
        'transOrigDiffSourceField' => 'l10n_diffsource',
        'delete' => 'deleted',
        'enablecolumns' => [
            'disabled' => 'hidden',
            'starttime' => 'starttime',
            'endtime' => 'endtime',
        ],
        'searchFields' => 'title,from_name,from_mail,from_subject,from_salutation,from_body,from_complimentary_close,from_footer,reply_to_name,reply_to_mail',
        'iconfile' => 'EXT:kitt3n_form/Resources/Public/Icons/tx_kitt3nform_domain_model_mailtosender.gif'
    ],
    'interface' => [
        'showRecordFieldList' => 'sys_language_uid, l10n_parent, l10n_diffsource, hidden, title, from_name, from_mail, from_subject, from_salutation, from_body, from_complimentary_close, from_footer, append_data, reply_to_name, reply_to_mail',
    ],
    'types' => [
        '1' => ['showitem' => 'sys_language_uid, l10n_parent, l10n_diffsource, hidden, title, from_name, from_mail, from_subject, from_salutation, from_body, from_complimentary_close, from_footer, append_data, reply_to_name, reply_to_mail, --div--;LLL:EXT:frontend/Resources/Private/Language/locallang_ttc.xlf:tabs.access, starttime, endtime'],
    ],
    'columns' => [
        'sys_language_uid' => [
            'exclude' => true,
            'label' => 'LLL:EXT:core/Resources/Private/Language/locallang_general.xlf:LGL.language',
            'config' => [
                'type' => 'select',
                'renderType' => 'selectSingle',
                'special' => 'languages',
                'items' => [
                    [
                        'LLL:EXT:core/Resources/Private/Language/locallang_general.xlf:LGL.allLanguages',
                        -1,
                        'flags-multiple'
                    ]
                ],
                'default' => 0,
            ],
        ],
        'l10n_parent' => [
            'displayCond' => 'FIELD:sys_language_uid:>:0',
            'exclude' => true,
            'label' => 'LLL:EXT:core/Resources/Private/Language/locallang_general.xlf:LGL.l18n_parent',
            'config' => [
                'type' => 'select',
                'renderType' => 'selectSingle',
                'default' => 0,
                'items' => [
                    ['', 0],
                ],
                'foreign_table' => 'tx_kitt3nform_domain_model_mailtosender',
                'foreign_table_where' => 'AND {#tx_kitt3nform_domain_model_mailtosender}.{#pid}=###CURRENT_PID### AND {#tx_kitt3nform_domain_model_mailtosender}.{#sys_language_uid} IN (-1,0)',
            ],
        ],
        'l10n_diffsource' => [
            'config' => [
                'type' => 'passthrough',
            ],
        ],
        't3ver_label' => [
            'label' => 'LLL:EXT:core/Resources/Private/Language/locallang_general.xlf:LGL.versionLabel',
            'config' => [
                'type' => 'input',
                'size' => 30,
                'max' => 255,
            ],
        ],
        'hidden' => [
            'exclude' => true,
            'label' => 'LLL:EXT:core/Resources/Private/Language/locallang_general.xlf:LGL.visible',
            'config' => [
                'type' => 'check',
                'renderType' => 'checkboxToggle',
                'items' => [
                    [
                        0 => '',
                        1 => '',
                        'invertStateDisplay' => true
                    ]
                ],
            ],
        ],
        'starttime' => [
            'exclude' => true,
            'label' => 'LLL:EXT:core/Resources/Private/Language/locallang_general.xlf:LGL.starttime',
            'config' => [
                'type' => 'input',
                'renderType' => 'inputDateTime',
                'eval' => 'datetime,int',
                'default' => 0,
                'behaviour' => [
                    'allowLanguageSynchronization' => true
                ]
            ],
        ],
        'endtime' => [
            'exclude' => true,
            'label' => 'LLL:EXT:core/Resources/Private/Language/locallang_general.xlf:LGL.endtime',
            'config' => [
                'type' => 'input',
                'renderType' => 'inputDateTime',
                'eval' => 'datetime,int',
                'default' => 0,
                'range' => [
                    'upper' => mktime(0, 0, 0, 1, 1, 2038)
                ],
                'behaviour' => [
                    'allowLanguageSynchronization' => true
                ]
            ],
        ],

        'title' => [
            'exclude' => false,
            'label' => 'LLL:EXT:kitt3n_form/Resources/Private/Language/locallang_db.xlf:tx_kitt3nform_domain_model_mailtosender.title',
            'config' => [
                'type' => 'input',
                'size' => 30,
                'eval' => 'trim,required'
            ],
        ],
        'from_name' => [
            'exclude' => false,
            'label' => 'LLL:EXT:kitt3n_form/Resources/Private/Language/locallang_db.xlf:tx_kitt3nform_domain_model_mailtosender.from_name',
            'config' => [
                'type' => 'input',
                'size' => 30,
                'eval' => 'trim,required'
            ],
        ],
        'from_mail' => [
            'exclude' => false,
            'label' => 'LLL:EXT:kitt3n_form/Resources/Private/Language/locallang_db.xlf:tx_kitt3nform_domain_model_mailtosender.from_mail',
            'config' => [
                'type' => 'input',
                'size' => 30,
                'eval' => 'trim,required'
            ],
        ],
        'from_subject' => [
            'exclude' => false,
            'label' => 'LLL:EXT:kitt3n_form/Resources/Private/Language/locallang_db.xlf:tx_kitt3nform_domain_model_mailtosender.from_subject',
            'config' => [
                'type' => 'input',
                'size' => 30,
                'eval' => 'trim,required'
            ],
        ],
        'from_salutation' => [
            'exclude' => false,
            'label' => 'LLL:EXT:kitt3n_form/Resources/Private/Language/locallang_db.xlf:tx_kitt3nform_domain_model_mailtosender.from_salutation',
            'config' => [
                'type' => 'input',
                'size' => 30,
                'eval' => 'trim,required'
            ],
        ],
        'from_body' => [
            'exclude' => false,
            'label' => 'LLL:EXT:kitt3n_form/Resources/Private/Language/locallang_db.xlf:tx_kitt3nform_domain_model_mailtosender.from_body',
            'config' => [
                'type' => 'text',
                'enableRichtext' => true,
                'richtextConfiguration' => 'default',
                'fieldControl' => [
                    'fullScreenRichtext' => [
                        'disabled' => false,
                    ],
                ],
                'cols' => 40,
                'rows' => 15,
                'eval' => 'trim,required',
            ],
            
        ],
        'from_complimentary_close' => [
            'exclude' => false,
            'label' => 'LLL:EXT:kitt3n_form/Resources/Private/Language/locallang_db.xlf:tx_kitt3nform_domain_model_mailtosender.from_complimentary_close',
            'config' => [
                'type' => 'text',
                'enableRichtext' => true,
                'richtextConfiguration' => 'default',
                'fieldControl' => [
                    'fullScreenRichtext' => [
                        'disabled' => false,
                    ],
                ],
                'cols' => 40,
                'rows' => 15,
                'eval' => 'trim,required',
            ],
            
        ],
        'from_footer' => [
            'exclude' => false,
            'label' => 'LLL:EXT:kitt3n_form/Resources/Private/Language/locallang_db.xlf:tx_kitt3nform_domain_model_mailtosender.from_footer',
            'config' => [
                'type' => 'text',
                'enableRichtext' => true,
                'richtextConfiguration' => 'default',
                'fieldControl' => [
                    'fullScreenRichtext' => [
                        'disabled' => false,
                    ],
                ],
                'cols' => 40,
                'rows' => 15,
                'eval' => 'trim,required',
            ],
            
        ],
        'append_data' => [
            'exclude' => false,
            'label' => 'LLL:EXT:kitt3n_form/Resources/Private/Language/locallang_db.xlf:tx_kitt3nform_domain_model_mailtosender.append_data',
            'config' => [
                'type' => 'check',
                'items' => [
                    '1' => [
                        '0' => 'LLL:EXT:lang/locallang_core.xlf:labels.enabled'
                    ]
                ],
                'default' => 0,
            ]
        ],
        'reply_to_name' => [
            'exclude' => false,
            'label' => 'LLL:EXT:kitt3n_form/Resources/Private/Language/locallang_db.xlf:tx_kitt3nform_domain_model_mailtosender.reply_to_name',
            'config' => [
                'type' => 'input',
                'size' => 30,
                'eval' => 'trim'
            ],
        ],
        'reply_to_mail' => [
            'exclude' => false,
            'label' => 'LLL:EXT:kitt3n_form/Resources/Private/Language/locallang_db.xlf:tx_kitt3nform_domain_model_mailtosender.reply_to_mail',
            'config' => [
                'type' => 'input',
                'size' => 30,
                'eval' => 'trim'
            ],
        ],
    
    ],
];
