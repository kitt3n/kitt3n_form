<?php
return [
    'ctrl' => [
        'title' => 'LLL:EXT:kitt3n_form/Resources/Private/Language/locallang_db.xlf:tx_kitt3nform_domain_model_form',
        'label' => 'title',
        'tstamp' => 'tstamp',
        'crdate' => 'crdate',
        'cruser_id' => 'cruser_id',
        'versioningWS' => true,
        'languageField' => 'sys_language_uid',
        'transOrigPointerField' => 'l10n_parent',
        'transOrigDiffSourceField' => 'l10n_diffsource',
        'delete' => 'deleted',
        'enablecolumns' => [
            'disabled' => 'hidden',
            'starttime' => 'starttime',
            'endtime' => 'endtime',
        ],
        'searchFields' => 'title,description,text_submit,text_confirm,field_sender_first_name,field_sender_name,field_sender_mail,storage_path,storage_confirm_path',
        'iconfile' => 'EXT:kitt3n_form/Resources/Public/Icons/tx_kitt3nform_domain_model_form.gif'
    ],
    'interface' => [
        'showRecordFieldList' => 'sys_language_uid, l10n_parent, l10n_diffsource, hidden, title, description, text_submit, text_confirm, field_sender_first_name, field_sender_name, field_sender_mail, storage_path, storage_confirm_path, step, mail_to_sender_confirm, mail_to_sender, mail_to_receiver',
    ],
    'types' => [
        '1' => ['showitem' => 'sys_language_uid, l10n_parent, l10n_diffsource, hidden, title, description, text_submit, text_confirm, field_sender_first_name, field_sender_name, field_sender_mail, storage_path, storage_confirm_path, step, mail_to_sender_confirm, mail_to_sender, mail_to_receiver, --div--;LLL:EXT:frontend/Resources/Private/Language/locallang_ttc.xlf:tabs.access, starttime, endtime'],
    ],
    'columns' => [
        'sys_language_uid' => [
            'exclude' => true,
            'label' => 'LLL:EXT:core/Resources/Private/Language/locallang_general.xlf:LGL.language',
            'config' => [
                'type' => 'select',
                'renderType' => 'selectSingle',
                'special' => 'languages',
                'items' => [
                    [
                        'LLL:EXT:core/Resources/Private/Language/locallang_general.xlf:LGL.allLanguages',
                        -1,
                        'flags-multiple'
                    ]
                ],
                'default' => 0,
            ],
        ],
        'l10n_parent' => [
            'displayCond' => 'FIELD:sys_language_uid:>:0',
            'exclude' => true,
            'label' => 'LLL:EXT:core/Resources/Private/Language/locallang_general.xlf:LGL.l18n_parent',
            'config' => [
                'type' => 'select',
                'renderType' => 'selectSingle',
                'default' => 0,
                'items' => [
                    ['', 0],
                ],
                'foreign_table' => 'tx_kitt3nform_domain_model_form',
                'foreign_table_where' => 'AND {#tx_kitt3nform_domain_model_form}.{#pid}=###CURRENT_PID### AND {#tx_kitt3nform_domain_model_form}.{#sys_language_uid} IN (-1,0)',
            ],
        ],
        'l10n_diffsource' => [
            'config' => [
                'type' => 'passthrough',
            ],
        ],
        't3ver_label' => [
            'label' => 'LLL:EXT:core/Resources/Private/Language/locallang_general.xlf:LGL.versionLabel',
            'config' => [
                'type' => 'input',
                'size' => 30,
                'max' => 255,
            ],
        ],
        'hidden' => [
            'exclude' => true,
            'label' => 'LLL:EXT:core/Resources/Private/Language/locallang_general.xlf:LGL.visible',
            'config' => [
                'type' => 'check',
                'renderType' => 'checkboxToggle',
                'items' => [
                    [
                        0 => '',
                        1 => '',
                        'invertStateDisplay' => true
                    ]
                ],
            ],
        ],
        'starttime' => [
            'exclude' => true,
            'label' => 'LLL:EXT:core/Resources/Private/Language/locallang_general.xlf:LGL.starttime',
            'config' => [
                'type' => 'input',
                'renderType' => 'inputDateTime',
                'eval' => 'datetime,int',
                'default' => 0,
                'behaviour' => [
                    'allowLanguageSynchronization' => true
                ]
            ],
        ],
        'endtime' => [
            'exclude' => true,
            'label' => 'LLL:EXT:core/Resources/Private/Language/locallang_general.xlf:LGL.endtime',
            'config' => [
                'type' => 'input',
                'renderType' => 'inputDateTime',
                'eval' => 'datetime,int',
                'default' => 0,
                'range' => [
                    'upper' => mktime(0, 0, 0, 1, 1, 2038)
                ],
                'behaviour' => [
                    'allowLanguageSynchronization' => true
                ]
            ],
        ],

        'title' => [
            'exclude' => false,
            'label' => 'LLL:EXT:kitt3n_form/Resources/Private/Language/locallang_db.xlf:tx_kitt3nform_domain_model_form.title',
            'config' => [
                'type' => 'input',
                'size' => 30,
                'eval' => 'trim,required'
            ],
        ],
        'description' => [
            'exclude' => false,
            'label' => 'LLL:EXT:kitt3n_form/Resources/Private/Language/locallang_db.xlf:tx_kitt3nform_domain_model_form.description',
            'config' => [
                'type' => 'text',
                'cols' => 40,
                'rows' => 15,
                'eval' => 'trim'
            ]
        ],
        'text_submit' => [
            'exclude' => false,
            'label' => 'LLL:EXT:kitt3n_form/Resources/Private/Language/locallang_db.xlf:tx_kitt3nform_domain_model_form.text_submit',
            'config' => [
                'type' => 'text',
                'enableRichtext' => true,
                'richtextConfiguration' => 'default',
                'fieldControl' => [
                    'fullScreenRichtext' => [
                        'disabled' => false,
                    ],
                ],
                'cols' => 40,
                'rows' => 15,
                'eval' => 'trim',
            ],
            
        ],
        'text_confirm' => [
            'exclude' => false,
            'label' => 'LLL:EXT:kitt3n_form/Resources/Private/Language/locallang_db.xlf:tx_kitt3nform_domain_model_form.text_confirm',
            'config' => [
                'type' => 'text',
                'enableRichtext' => true,
                'richtextConfiguration' => 'default',
                'fieldControl' => [
                    'fullScreenRichtext' => [
                        'disabled' => false,
                    ],
                ],
                'cols' => 40,
                'rows' => 15,
                'eval' => 'trim',
            ],
            
        ],
        'field_sender_first_name' => [
            'exclude' => false,
            'label' => 'LLL:EXT:kitt3n_form/Resources/Private/Language/locallang_db.xlf:tx_kitt3nform_domain_model_form.field_sender_first_name',
            'config' => [
                'type' => 'input',
                'size' => 30,
                'eval' => 'trim'
            ],
        ],
        'field_sender_name' => [
            'exclude' => false,
            'label' => 'LLL:EXT:kitt3n_form/Resources/Private/Language/locallang_db.xlf:tx_kitt3nform_domain_model_form.field_sender_name',
            'config' => [
                'type' => 'input',
                'size' => 30,
                'eval' => 'trim'
            ],
        ],
        'field_sender_mail' => [
            'exclude' => false,
            'label' => 'LLL:EXT:kitt3n_form/Resources/Private/Language/locallang_db.xlf:tx_kitt3nform_domain_model_form.field_sender_mail',
            'config' => [
                'type' => 'input',
                'size' => 30,
                'eval' => 'trim'
            ],
        ],
        'storage_path' => [
            'exclude' => false,
            'label' => 'LLL:EXT:kitt3n_form/Resources/Private/Language/locallang_db.xlf:tx_kitt3nform_domain_model_form.storage_path',
            'config' => [
                'type' => 'text',
                'cols' => 40,
                'rows' => 15,
                'eval' => 'trim'
            ]
        ],
        'storage_confirm_path' => [
            'exclude' => false,
            'label' => 'LLL:EXT:kitt3n_form/Resources/Private/Language/locallang_db.xlf:tx_kitt3nform_domain_model_form.storage_confirm_path',
            'config' => [
                'type' => 'text',
                'cols' => 40,
                'rows' => 15,
                'eval' => 'trim'
            ]
        ],
        'step' => [
            'exclude' => false,
            'label' => 'LLL:EXT:kitt3n_form/Resources/Private/Language/locallang_db.xlf:tx_kitt3nform_domain_model_form.step',
            'config' => [
                'type' => 'select',
                'renderType' => 'selectMultipleSideBySide',
                'foreign_table' => 'tx_kitt3nform_domain_model_step',
                'MM' => 'tx_kitt3nform_form_step_mm',
                'size' => 10,
                'autoSizeMax' => 30,
                'maxitems' => 9999,
                'multiple' => 0,
                'fieldControl' => [
                    'editPopup' => [
                        'disabled' => false,
                    ],
                    'addRecord' => [
                        'disabled' => false,
                    ],
                    'listModule' => [
                        'disabled' => true,
                    ],
                ],
            ],
            
        ],
        'mail_to_sender_confirm' => [
            'exclude' => false,
            'label' => 'LLL:EXT:kitt3n_form/Resources/Private/Language/locallang_db.xlf:tx_kitt3nform_domain_model_form.mail_to_sender_confirm',
            'config' => [
                'type' => 'select',
                'renderType' => 'selectSingle',
                'foreign_table' => 'tx_kitt3nform_domain_model_mailtosenderconfirm',
                'minitems' => 0,
                'maxitems' => 1,
            ],
        ],
        'mail_to_sender' => [
            'exclude' => false,
            'label' => 'LLL:EXT:kitt3n_form/Resources/Private/Language/locallang_db.xlf:tx_kitt3nform_domain_model_form.mail_to_sender',
            'config' => [
                'type' => 'select',
                'renderType' => 'selectSingle',
                'foreign_table' => 'tx_kitt3nform_domain_model_mailtosender',
                'minitems' => 0,
                'maxitems' => 1,
            ],
        ],
        'mail_to_receiver' => [
            'exclude' => false,
            'label' => 'LLL:EXT:kitt3n_form/Resources/Private/Language/locallang_db.xlf:tx_kitt3nform_domain_model_form.mail_to_receiver',
            'config' => [
                'type' => 'select',
                'renderType' => 'selectSingle',
                'foreign_table' => 'tx_kitt3nform_domain_model_mailtoreceiver',
                'minitems' => 0,
                'maxitems' => 1,
            ],
        ],
    
    ],
];
