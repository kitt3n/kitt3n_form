<?php
return [
    'ctrl' => [
        'title' => 'LLL:EXT:kitt3n_form/Resources/Private/Language/locallang_db.xlf:tx_kitt3nform_domain_model_constraint',
        'label' => 'mandatory',
        'tstamp' => 'tstamp',
        'crdate' => 'crdate',
        'cruser_id' => 'cruser_id',
        'versioningWS' => true,
        'languageField' => 'sys_language_uid',
        'transOrigPointerField' => 'l10n_parent',
        'transOrigDiffSourceField' => 'l10n_diffsource',
        'delete' => 'deleted',
        'enablecolumns' => [
            'disabled' => 'hidden',
            'starttime' => 'starttime',
            'endtime' => 'endtime',
        ],
        'searchFields' => 'mime_type,max_file_size_unit',
        'iconfile' => 'EXT:kitt3n_form/Resources/Public/Icons/tx_kitt3nform_domain_model_constraint.gif'
    ],
    'interface' => [
        'showRecordFieldList' => 'sys_language_uid, l10n_parent, l10n_diffsource, hidden, mandatory, min_length, max_length, mime_type, max_file_size, max_file_size_unit, form, field',
    ],
    'types' => [
        '1' => ['showitem' => 'sys_language_uid, l10n_parent, l10n_diffsource, hidden, mandatory, min_length, max_length, mime_type, max_file_size, max_file_size_unit, form, field, --div--;LLL:EXT:frontend/Resources/Private/Language/locallang_ttc.xlf:tabs.access, starttime, endtime'],
    ],
    'columns' => [
        'sys_language_uid' => [
            'exclude' => true,
            'label' => 'LLL:EXT:core/Resources/Private/Language/locallang_general.xlf:LGL.language',
            'config' => [
                'type' => 'select',
                'renderType' => 'selectSingle',
                'special' => 'languages',
                'items' => [
                    [
                        'LLL:EXT:core/Resources/Private/Language/locallang_general.xlf:LGL.allLanguages',
                        -1,
                        'flags-multiple'
                    ]
                ],
                'default' => 0,
            ],
        ],
        'l10n_parent' => [
            'displayCond' => 'FIELD:sys_language_uid:>:0',
            'exclude' => true,
            'label' => 'LLL:EXT:core/Resources/Private/Language/locallang_general.xlf:LGL.l18n_parent',
            'config' => [
                'type' => 'select',
                'renderType' => 'selectSingle',
                'default' => 0,
                'items' => [
                    ['', 0],
                ],
                'foreign_table' => 'tx_kitt3nform_domain_model_constraint',
                'foreign_table_where' => 'AND {#tx_kitt3nform_domain_model_constraint}.{#pid}=###CURRENT_PID### AND {#tx_kitt3nform_domain_model_constraint}.{#sys_language_uid} IN (-1,0)',
            ],
        ],
        'l10n_diffsource' => [
            'config' => [
                'type' => 'passthrough',
            ],
        ],
        't3ver_label' => [
            'label' => 'LLL:EXT:core/Resources/Private/Language/locallang_general.xlf:LGL.versionLabel',
            'config' => [
                'type' => 'input',
                'size' => 30,
                'max' => 255,
            ],
        ],
        'hidden' => [
            'exclude' => true,
            'label' => 'LLL:EXT:core/Resources/Private/Language/locallang_general.xlf:LGL.visible',
            'config' => [
                'type' => 'check',
                'renderType' => 'checkboxToggle',
                'items' => [
                    [
                        0 => '',
                        1 => '',
                        'invertStateDisplay' => true
                    ]
                ],
            ],
        ],
        'starttime' => [
            'exclude' => true,
            'label' => 'LLL:EXT:core/Resources/Private/Language/locallang_general.xlf:LGL.starttime',
            'config' => [
                'type' => 'input',
                'renderType' => 'inputDateTime',
                'eval' => 'datetime,int',
                'default' => 0,
                'behaviour' => [
                    'allowLanguageSynchronization' => true
                ]
            ],
        ],
        'endtime' => [
            'exclude' => true,
            'label' => 'LLL:EXT:core/Resources/Private/Language/locallang_general.xlf:LGL.endtime',
            'config' => [
                'type' => 'input',
                'renderType' => 'inputDateTime',
                'eval' => 'datetime,int',
                'default' => 0,
                'range' => [
                    'upper' => mktime(0, 0, 0, 1, 1, 2038)
                ],
                'behaviour' => [
                    'allowLanguageSynchronization' => true
                ]
            ],
        ],

        'mandatory' => [
            'exclude' => false,
            'label' => 'LLL:EXT:kitt3n_form/Resources/Private/Language/locallang_db.xlf:tx_kitt3nform_domain_model_constraint.mandatory',
            'config' => [
                'type' => 'check',
                'items' => [
                    '1' => [
                        '0' => 'LLL:EXT:lang/locallang_core.xlf:labels.enabled'
                    ]
                ],
                'default' => 0,
            ]
        ],
        'min_length' => [
            'exclude' => false,
            'label' => 'LLL:EXT:kitt3n_form/Resources/Private/Language/locallang_db.xlf:tx_kitt3nform_domain_model_constraint.min_length',
            'config' => [
                'type' => 'input',
                'size' => 4,
                'eval' => 'int'
            ]
        ],
        'max_length' => [
            'exclude' => false,
            'label' => 'LLL:EXT:kitt3n_form/Resources/Private/Language/locallang_db.xlf:tx_kitt3nform_domain_model_constraint.max_length',
            'config' => [
                'type' => 'input',
                'size' => 4,
                'eval' => 'int'
            ]
        ],
        'mime_type' => [
            'exclude' => false,
            'label' => 'LLL:EXT:kitt3n_form/Resources/Private/Language/locallang_db.xlf:tx_kitt3nform_domain_model_constraint.mime_type',
            'config' => [
                'type' => 'input',
                'size' => 30,
                'eval' => 'trim'
            ],
        ],
        'max_file_size' => [
            'exclude' => false,
            'label' => 'LLL:EXT:kitt3n_form/Resources/Private/Language/locallang_db.xlf:tx_kitt3nform_domain_model_constraint.max_file_size',
            'config' => [
                'type' => 'input',
                'size' => 30,
                'eval' => 'double2'
            ]
        ],
        'max_file_size_unit' => [
            'exclude' => false,
            'label' => 'LLL:EXT:kitt3n_form/Resources/Private/Language/locallang_db.xlf:tx_kitt3nform_domain_model_constraint.max_file_size_unit',
            'config' => [
                'type' => 'input',
                'size' => 30,
                'eval' => 'trim'
            ],
        ],
        'form' => [
            'exclude' => false,
            'label' => 'LLL:EXT:kitt3n_form/Resources/Private/Language/locallang_db.xlf:tx_kitt3nform_domain_model_constraint.form',
            'config' => [
                'type' => 'inline',
                'foreign_table' => 'tx_kitt3nform_domain_model_form',
                'minitems' => 0,
                'maxitems' => 1,
                'appearance' => [
                    'collapseAll' => 0,
                    'levelLinksPosition' => 'top',
                    'showSynchronizationLink' => 1,
                    'showPossibleLocalizationRecords' => 1,
                    'showAllLocalizationLink' => 1
                ],
            ],
        ],
        'field' => [
            'exclude' => false,
            'label' => 'LLL:EXT:kitt3n_form/Resources/Private/Language/locallang_db.xlf:tx_kitt3nform_domain_model_constraint.field',
            'config' => [
                'type' => 'inline',
                'foreign_table' => 'tx_kitt3nform_domain_model_field',
                'minitems' => 0,
                'maxitems' => 1,
                'appearance' => [
                    'collapseAll' => 0,
                    'levelLinksPosition' => 'top',
                    'showSynchronizationLink' => 1,
                    'showPossibleLocalizationRecords' => 1,
                    'showAllLocalizationLink' => 1
                ],
            ],
        ],
    
    ],
];
