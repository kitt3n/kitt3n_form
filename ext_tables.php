<?php
defined('TYPO3_MODE') || die('Access denied.');

call_user_func(
    function()
    {

        \TYPO3\CMS\Extbase\Utility\ExtensionUtility::registerPlugin(
            'KITT3N.Kitt3nForm',
            'Kitt3nformformrender',
            'kitt3n_form :: Form :: render'
        );

        \TYPO3\CMS\Extbase\Utility\ExtensionUtility::registerPlugin(
            'KITT3N.Kitt3nForm',
            'Kitt3nformrenderrender',
            'kitt3n_form :: Render :: render'
        );

        \TYPO3\CMS\Core\Utility\ExtensionManagementUtility::addStaticFile('kitt3n_form', 'Configuration/TypoScript', 'kitt3n_form');

        \TYPO3\CMS\Core\Utility\ExtensionManagementUtility::addLLrefForTCAdescr('tx_kitt3nform_domain_model_form', 'EXT:kitt3n_form/Resources/Private/Language/locallang_csh_tx_kitt3nform_domain_model_form.xlf');
        \TYPO3\CMS\Core\Utility\ExtensionManagementUtility::allowTableOnStandardPages('tx_kitt3nform_domain_model_form');

        \TYPO3\CMS\Core\Utility\ExtensionManagementUtility::addLLrefForTCAdescr('tx_kitt3nform_domain_model_field', 'EXT:kitt3n_form/Resources/Private/Language/locallang_csh_tx_kitt3nform_domain_model_field.xlf');
        \TYPO3\CMS\Core\Utility\ExtensionManagementUtility::allowTableOnStandardPages('tx_kitt3nform_domain_model_field');

        \TYPO3\CMS\Core\Utility\ExtensionManagementUtility::addLLrefForTCAdescr('tx_kitt3nform_domain_model_fieldset', 'EXT:kitt3n_form/Resources/Private/Language/locallang_csh_tx_kitt3nform_domain_model_fieldset.xlf');
        \TYPO3\CMS\Core\Utility\ExtensionManagementUtility::allowTableOnStandardPages('tx_kitt3nform_domain_model_fieldset');

        \TYPO3\CMS\Core\Utility\ExtensionManagementUtility::addLLrefForTCAdescr('tx_kitt3nform_domain_model_step', 'EXT:kitt3n_form/Resources/Private/Language/locallang_csh_tx_kitt3nform_domain_model_step.xlf');
        \TYPO3\CMS\Core\Utility\ExtensionManagementUtility::allowTableOnStandardPages('tx_kitt3nform_domain_model_step');

        \TYPO3\CMS\Core\Utility\ExtensionManagementUtility::addLLrefForTCAdescr('tx_kitt3nform_domain_model_render', 'EXT:kitt3n_form/Resources/Private/Language/locallang_csh_tx_kitt3nform_domain_model_render.xlf');
        \TYPO3\CMS\Core\Utility\ExtensionManagementUtility::allowTableOnStandardPages('tx_kitt3nform_domain_model_render');

        \TYPO3\CMS\Core\Utility\ExtensionManagementUtility::addLLrefForTCAdescr('tx_kitt3nform_domain_model_constraint', 'EXT:kitt3n_form/Resources/Private/Language/locallang_csh_tx_kitt3nform_domain_model_constraint.xlf');
        \TYPO3\CMS\Core\Utility\ExtensionManagementUtility::allowTableOnStandardPages('tx_kitt3nform_domain_model_constraint');

        \TYPO3\CMS\Core\Utility\ExtensionManagementUtility::addLLrefForTCAdescr('tx_kitt3nform_domain_model_mailtosender', 'EXT:kitt3n_form/Resources/Private/Language/locallang_csh_tx_kitt3nform_domain_model_mailtosender.xlf');
        \TYPO3\CMS\Core\Utility\ExtensionManagementUtility::allowTableOnStandardPages('tx_kitt3nform_domain_model_mailtosender');

        \TYPO3\CMS\Core\Utility\ExtensionManagementUtility::addLLrefForTCAdescr('tx_kitt3nform_domain_model_mailtosenderconfirm', 'EXT:kitt3n_form/Resources/Private/Language/locallang_csh_tx_kitt3nform_domain_model_mailtosenderconfirm.xlf');
        \TYPO3\CMS\Core\Utility\ExtensionManagementUtility::allowTableOnStandardPages('tx_kitt3nform_domain_model_mailtosenderconfirm');

        \TYPO3\CMS\Core\Utility\ExtensionManagementUtility::addLLrefForTCAdescr('tx_kitt3nform_domain_model_mailtoreceiver', 'EXT:kitt3n_form/Resources/Private/Language/locallang_csh_tx_kitt3nform_domain_model_mailtoreceiver.xlf');
        \TYPO3\CMS\Core\Utility\ExtensionManagementUtility::allowTableOnStandardPages('tx_kitt3nform_domain_model_mailtoreceiver');

    }
);
## EXTENSION BUILDER DEFAULTS END TOKEN - Everything BEFORE this line is overwritten with the defaults of the extension builder

$extensionName = strtolower(\TYPO3\CMS\Core\Utility\GeneralUtility::underscoredToUpperCamelCase($_EXTKEY));
$pluginName = strtolower('Kitt3nformrenderrender');
$pluginSignature = $extensionName.'_'.$pluginName;
$TCA['tt_content']['types']['list']['subtypes_excludelist'][$pluginSignature] = 'layout,select_key,pages,recursive';
$TCA['tt_content']['types']['list']['subtypes_addlist'][$pluginSignature] = 'pi_flexform';
\TYPO3\CMS\Core\Utility\ExtensionManagementUtility::addPiFlexFormValue($pluginSignature, 'FILE:EXT:'.$_EXTKEY . '/Configuration/FlexForms/Config.xml');