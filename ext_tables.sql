#
# Table structure for table 'tx_kitt3nform_domain_model_form'
#
CREATE TABLE tx_kitt3nform_domain_model_form (

	title varchar(255) DEFAULT '' NOT NULL,
	description text,
	text_submit text,
	text_confirm text,
	field_sender_first_name varchar(255) DEFAULT '' NOT NULL,
	field_sender_name varchar(255) DEFAULT '' NOT NULL,
	field_sender_mail varchar(255) DEFAULT '' NOT NULL,
	storage_path text,
	storage_confirm_path text,
	step int(11) unsigned DEFAULT '0' NOT NULL,
	mail_to_sender_confirm int(11) unsigned DEFAULT '0',
	mail_to_sender int(11) unsigned DEFAULT '0',
	mail_to_receiver int(11) unsigned DEFAULT '0',

);

#
# Table structure for table 'tx_kitt3nform_domain_model_field'
#
CREATE TABLE tx_kitt3nform_domain_model_field (

	title varchar(255) DEFAULT '' NOT NULL,
	type varchar(255) DEFAULT '' NOT NULL,
	rich_text_label text,
	options text,
	help_text varchar(255) DEFAULT '' NOT NULL,
	class varchar(255) DEFAULT '' NOT NULL,
	placeholder varchar(255) DEFAULT '' NOT NULL,

);

#
# Table structure for table 'tx_kitt3nform_domain_model_fieldset'
#
CREATE TABLE tx_kitt3nform_domain_model_fieldset (

	title varchar(255) DEFAULT '' NOT NULL,
	class varchar(255) DEFAULT '' NOT NULL,
	field int(11) unsigned DEFAULT '0' NOT NULL,

);

#
# Table structure for table 'tx_kitt3nform_domain_model_step'
#
CREATE TABLE tx_kitt3nform_domain_model_step (

	title varchar(255) DEFAULT '' NOT NULL,
	fieldset int(11) unsigned DEFAULT '0' NOT NULL,

);

#
# Table structure for table 'tx_kitt3nform_domain_model_render'
#
CREATE TABLE tx_kitt3nform_domain_model_render (

	dummy varchar(255) DEFAULT '' NOT NULL,

);

#
# Table structure for table 'tx_kitt3nform_domain_model_constraint'
#
CREATE TABLE tx_kitt3nform_domain_model_constraint (

	mandatory smallint(5) unsigned DEFAULT '0' NOT NULL,
	min_length int(11) DEFAULT '0' NOT NULL,
	max_length int(11) DEFAULT '0' NOT NULL,
	mime_type varchar(255) DEFAULT '' NOT NULL,
	max_file_size double(11,2) DEFAULT '0.00' NOT NULL,
	max_file_size_unit varchar(255) DEFAULT '' NOT NULL,
	form int(11) unsigned DEFAULT '0',
	field int(11) unsigned DEFAULT '0',

);

#
# Table structure for table 'tx_kitt3nform_domain_model_mailtosender'
#
CREATE TABLE tx_kitt3nform_domain_model_mailtosender (

	title varchar(255) DEFAULT '' NOT NULL,
	from_name varchar(255) DEFAULT '' NOT NULL,
	from_mail varchar(255) DEFAULT '' NOT NULL,
	from_subject varchar(255) DEFAULT '' NOT NULL,
	from_salutation varchar(255) DEFAULT '' NOT NULL,
	from_body text,
	from_complimentary_close text,
	from_footer text,
	append_data smallint(5) unsigned DEFAULT '0' NOT NULL,
	reply_to_name varchar(255) DEFAULT '' NOT NULL,
	reply_to_mail varchar(255) DEFAULT '' NOT NULL,

);

#
# Table structure for table 'tx_kitt3nform_domain_model_mailtosenderconfirm'
#
CREATE TABLE tx_kitt3nform_domain_model_mailtosenderconfirm (

	title varchar(255) DEFAULT '' NOT NULL,
	from_name varchar(255) DEFAULT '' NOT NULL,
	from_mail varchar(255) DEFAULT '' NOT NULL,
	from_subject varchar(255) DEFAULT '' NOT NULL,
	from_salutation varchar(255) DEFAULT '' NOT NULL,
	from_body_before_link text,
	from_body_after_link text,
	from_complimentary_close text,
	from_footer text,
	reply_to_name varchar(255) DEFAULT '' NOT NULL,
	reply_to_mail varchar(255) DEFAULT '' NOT NULL,

);

#
# Table structure for table 'tx_kitt3nform_domain_model_mailtoreceiver'
#
CREATE TABLE tx_kitt3nform_domain_model_mailtoreceiver (

	title varchar(255) DEFAULT '' NOT NULL,
	from_name varchar(255) DEFAULT '' NOT NULL,
	from_mail varchar(255) DEFAULT '' NOT NULL,
	from_subject varchar(255) DEFAULT '' NOT NULL,
	to_name varchar(255) DEFAULT '' NOT NULL,
	to_mail varchar(255) DEFAULT '' NOT NULL,
	to_cc varchar(255) DEFAULT '' NOT NULL,
	to_bcc varchar(255) DEFAULT '' NOT NULL,
	append_data smallint(5) unsigned DEFAULT '0' NOT NULL,

);

#
# Table structure for table 'tx_kitt3nform_form_step_mm'
#
CREATE TABLE tx_kitt3nform_form_step_mm (
	uid_local int(11) unsigned DEFAULT '0' NOT NULL,
	uid_foreign int(11) unsigned DEFAULT '0' NOT NULL,
	sorting int(11) unsigned DEFAULT '0' NOT NULL,
	sorting_foreign int(11) unsigned DEFAULT '0' NOT NULL,

	PRIMARY KEY (uid_local,uid_foreign),
	KEY uid_local (uid_local),
	KEY uid_foreign (uid_foreign)
);

#
# Table structure for table 'tx_kitt3nform_fieldset_field_mm'
#
CREATE TABLE tx_kitt3nform_fieldset_field_mm (
	uid_local int(11) unsigned DEFAULT '0' NOT NULL,
	uid_foreign int(11) unsigned DEFAULT '0' NOT NULL,
	sorting int(11) unsigned DEFAULT '0' NOT NULL,
	sorting_foreign int(11) unsigned DEFAULT '0' NOT NULL,

	PRIMARY KEY (uid_local,uid_foreign),
	KEY uid_local (uid_local),
	KEY uid_foreign (uid_foreign)
);

#
# Table structure for table 'tx_kitt3nform_step_fieldset_mm'
#
CREATE TABLE tx_kitt3nform_step_fieldset_mm (
	uid_local int(11) unsigned DEFAULT '0' NOT NULL,
	uid_foreign int(11) unsigned DEFAULT '0' NOT NULL,
	sorting int(11) unsigned DEFAULT '0' NOT NULL,
	sorting_foreign int(11) unsigned DEFAULT '0' NOT NULL,

	PRIMARY KEY (uid_local,uid_foreign),
	KEY uid_local (uid_local),
	KEY uid_foreign (uid_foreign)
);

## EXTENSION BUILDER DEFAULTS END TOKEN - Everything BEFORE this line is overwritten with the defaults of the extension builder

#
# Table structure for table 'tx_kitt3nform_domain_model_constraint'
#
CREATE TABLE tx_kitt3nform_domain_model_constraint (
	mime_type varchar(10000) DEFAULT '' NOT NULL,
);


#
# Table structure for table 'tx_kitt3nform_domain_model_form'
#
CREATE TABLE tx_kitt3nform_domain_model_form (

	KEY step_idx (step),
	KEY mail_to_sender_confirm_idx (mail_to_sender_confirm),
	KEY mail_to_sender_idx (mail_to_sender),
	KEY mail_to_receiver_idx (mail_to_receiver)

);

#
# Table structure for table 'tx_kitt3nform_domain_model_fieldset'
#
CREATE TABLE tx_kitt3nform_domain_model_fieldset (

	KEY field_idx (field)

);

#
# Table structure for table 'tx_kitt3nform_domain_model_step'
#
CREATE TABLE tx_kitt3nform_domain_model_step (

  KEY fieldset_idx (fieldset)

);

#
# Table structure for table 'tx_kitt3nform_domain_model_constraint'
#
CREATE TABLE tx_kitt3nform_domain_model_constraint (

	KEY field_idx (field),
	KEY form_idx (form)

);